##############################################
# STAGE 1: build jsonnet 
##############################################

FROM alpine:3.10 AS builder
ENV JSONNET_VERSION v0.13.0
RUN apk -U add build-base git
WORKDIR /opt
RUN git clone -b ${JSONNET_VERSION} https://github.com/google/jsonnet.git /opt/jsonnet
RUN cd jsonnet &&  make


##############################################
# STAGE 2: build ksonnet 
##############################################

FROM golang:1.11-alpine AS builder-ksonnet
ENV KSONNET_VERSION v0.13.1
ARG LD_FLAGS
RUN apk -U add git
RUN git clone -b ${KSONNET_VERSION} https://github.com/ksonnet/ksonnet.git /tmp/ksonnet/
WORKDIR /go/src/github.com/ksonnet/ksonnet
RUN cp -r /tmp/ksonnet/*  /go/src/github.com/ksonnet/ksonnet
RUN CGO_ENABLED=0 GOOS=linux go build -o ks -ldflags="${LD_FLAGS} -s -w" ./cmd/ks


##############################################
# STAGE 3 build main image. 
##############################################

FROM docker:19.03.11

ENV KUBECFG_VERSION v0.12.0
ENV JSONNET_VERSION v0.13.0
ENV KUBE_LATEST_VERSION v1.15.12
ENV JSONNET_VERSION v0.13.0
ENV JQ_VERSION jq-1.6
ENV YQ_VERSION 2.4.0
ENV KUBECFG_JPATH /usr/share/kubecfg/${KUBECFG_VERSION}

LABEL maintainer="Lebedev Nikolay"

ARG VCS_REF
ARG BUILD_DATE

# Metadata
LABEL org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://github.com/lachie83/k8s-kubectl" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.docker.dockerfile="/Dockerfile"

#kubectl
RUN apk add --update ca-certificates curl bash git openssh openssh-client openssl gnutls-utils libstdc++ \
 && curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
 && chmod +x /usr/local/bin/kubectl \
 && rm /var/cache/apk/*

#kubecfg
RUN mkdir -p /usr/share/kubecfg/${KUBECFG_VERSION}
RUN curl -L https://github.com/bitnami/kubecfg/releases/download/${KUBECFG_VERSION}/kubecfg-linux-amd64 -o /usr/local/bin/kubecfg \
    && chmod +x /usr/local/bin/kubecfg
RUN curl -L https://github.com/bitnami/kubecfg/blob/master/lib/kubecfg.libsonnet -o /usr/share/kubecfg/${KUBECFG_VERSION}/kubecfg.libsonnet 

#jsonnet
COPY --from=builder /opt/jsonnet/jsonnet /usr/local/bin
COPY --from=builder /opt/jsonnet/jsonnetfmt /usr/local/bin

#ksonnet
COPY --from=builder-ksonnet /go/src/github.com/ksonnet/ksonnet/ks /usr/local/bin

# Get ksonnet-lib, kube.libsonnet add to the Jsonnet -J path.
RUN git clone https://github.com/ksonnet/ksonnet-lib.git
RUN mkdir -p /usr/share/${JSONNET_VERSION}
RUN cp -r ksonnet-lib/ksonnet.beta.4 /usr/share/${JSONNET_VERSION}
RUN curl -L https://github.com/bitnami-labs/kube-libsonnet/blob/master/kube.libsonnet -o /usr/share/${JSONNET_VERSION}/kube.libsonnet

#jq,yq parse tools
RUN curl -L https://github.com/stedolan/jq/releases/download/${JQ_VERSION}/jq-linux64 -o /usr/local/bin/jq \
    && chmod +x /usr/local/bin/jq
RUN curl -L https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64 -o /usr/local/bin/yq \
    && chmod +x /usr/local/bin/yq

#helm3

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
RUN chmod 700 get_helm.sh
RUN ./get_helm.sh

#gcloud
ARG CLOUD_SDK_VERSION=305.0.0
ENV CLOUD_SDK_VERSION=$CLOUD_SDK_VERSION
ENV CLOUDSDK_PYTHON=python3
ENV PATH /google-cloud-sdk/bin:$PATH
COPY --from=docker:19.03.11 /usr/local/bin/docker /usr/local/bin/docker
RUN apk --no-cache add \
        curl \
        python3 \
        py3-crcmod \
        bash \
        libc6-compat \
        openssh-client \
        git \
        gnupg \
    && curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    tar xzf google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    rm google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    gcloud config set core/disable_usage_reporting true && \
    gcloud config set component_manager/disable_update_check true && \
    gcloud config set metrics/environment github_docker_image && \
    gcloud --version
RUN git config --system credential.'https://source.developers.google.com'.helper gcloud.sh

RUN mkdir -p /usr/lib/google-cloud-sdk/bin
RUN ln -s /google-cloud-sdk/bin/gcloud /usr/lib/google-cloud-sdk/bin

WORKDIR /root
