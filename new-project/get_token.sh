
kubectl apply -f ns.yaml

kubectl apply -n nodejs -f role.yaml

kubectl -n nodejs create sa  node-account

kubectl -n nodejs apply -f  rb.yaml

secret=$(kubectl -n nodejs get sa node-account -o json | jq -r .secrets[].name)

kubectl -n nodejs get secret $secret -o json | jq -r '.data["ca.crt"]' | base64 --decode > ca.crt

user_token=$(kubectl -n nodejs  get secret $secret -o json | jq -r '.data["token"]' | base64 --decode)

echo $user_token

